import json
import matplotlib.pyplot as plt
import numpy as np  

# Importa dados da simulação catenoidejs
with open('data.json') as f:
    data = json.load(f)
    

































# Variaveis disponiveis para análise
options = ['simulationTime','delta_x_spring', 'measureDeviceDistance', 'springForce', 'forceLJ','springPotential','kneticEnergy','potentialLJ','totalEnergy','x(t)','a(t)']

# Flag para loop
flag = ""

while (False):

    print("(1) Plotar combinações")
    print("(2) Energias")
    choice = input("")


    if (choice == "1"):
        print("Escolha duas opções:")
        print("-------------------------------\n")

        # Printa na tela as opções
        for i in options:
            print("(" + str(options.index(i)) + ") " + str(i))

        # Recebe a opção de cada eixo
        eixoX = int(input("\nEixo X: \n"))
        eixoY = int(input("\nEixo Y: \n"))
        
        
        if ((options[eixoY] == "springForce") and (options[eixoX] == "measureDeviceDistance")):
            
            x = data['measureDeviceDistance']
            plt.scatter(x, data['springForce'], c='r', marker='.', label='springForce')
            plt.scatter(x, data['forceLJ'], c='b', marker='.', label='forceLJ')
            
            plt.xlabel(options[eixoX])
            plt.title('Force measurement')
            plt.legend()
            plt.show()
            
        else:
            # Busca dentro do vetor options o valor escolhido para cada eixo plotando eles.
            plt.scatter(data[options[eixoX]],data[options[eixoY]])
            plt.xlabel(options[eixoX])
            plt.ylabel(options[eixoY])
            plt.show()

        print('Deseja realizar outra análise ?\n')
        flag = input("Sim (Enter)   Não (Qualquer outra tecla + Enter)")

    elif (choice == "2"):
        x = data['simulationTime']
        
        plt.plot(x,data['springPotential'] , c='b', marker='.', label='springPotential')
        plt.plot(x, data['kneticEnergy'], c='r', marker='.', label='kneticEnergy')
        plt.plot(x, data['potentialLJ'], c='g', marker='.', label='potentialLJ')

        plt.plot(x, data['totalEnergy'], c='black', marker='.', label='totalEnergy')
        plt.xlabel(options[0])
        plt.legend()
        plt.ylabel("Energy")
        plt.show()


        print('Deseja realizar outra análise ?\n')
        flag = input("Sim (Enter)   Não (Qualquer outra tecla + Enter)")



    else:
        print("Escolha uma opção válida")









def later():
    # Variáveis auxiliares
    # Guarda o valor do elemento anterior do eixoX
    aux = data[options[eixoX]][0]
    # Guarda o valor do elemento anterior do eixoY
    aux2 = data[options[eixoY]][0]

    # Contadores
    # Qual o número da iteração atual
    count = 0
    # Relacionado a média dos valores do eixoY para cada valor de eixoX diferente
    count2 = 0
    # Quantos números foram somados para utilizar como denominador no cálculo da média
    count3 = 0

    # Inicialização dos novos eixos
    # Novo eixoX
    measureDeviceDistance = [aux]
    # Novo eixoY
    springForce = [aux2]

    for i in data[options[eixoX]]:
        springForce[count2] += data[options[eixoY]][count]
        count += 1
        count3 += 1
        if (i != aux):
            measureDeviceDistance.append(i)
            aux = i

            springForce[count2] = springForce[count2]/count3
            count2 += 1
            count3 = 0
            springForce.append(0)

    # Busca dentro do vetor options o valor escolhido para cada eixo plotando eles.