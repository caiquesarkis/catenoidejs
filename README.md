# Nanocatenoide

**Visite**  https://caiquesarkis.gitlab.io/catenoidejs 

**Autores** Caique Sarkis ([@caique_sarkis](https://twitter.com/caique_sarkis)) e Julien Chopin

**Descrição** Simulação de dinâmica molecular que simula a dinâmica de partículas que interagem através do potencial de Lennard Jones. Aqui pode-se analisar um sistema análogo à uma catenóide em 1-D.

**Instruções**
- Para iniciar a simulação aperte em `start`.
- Para aumentar a intensidade do potencial elástico altere o valor do slider `Elastic potential intensity`.
- Para mudar a posição da partícula com o ponto preto altere o valor do slider `Equilibrium point`.
- Para baixar um arquivo JSON com informações sobre a evolução do sistema aperte em `Record data`. Baixe o arquivo quando achar que o tempo foi suficiente.
- Para fazer alguma partícula estática clique em cima dela.


## Referências

[1] [Potencial de Lennard Jones](https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Physical_Properties_of_Matter/Atomic_and_Molecular_Properties/Intermolecular_Forces/Specific_Interactions/Lennard-Jones_Potential)   

[2] [Integração de Verlet](https://www.algorithm-archive.org/contents/verlet_integration/verlet_integration.html)
