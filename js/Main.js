let jsonData = { 'simulationTime': [],
                 'x': [],
                 'y': [],
                 'Acc_x': [],
                 'k': [],
                 'molecularMass': [],
                 'sigma': [],
                 'epsilon': [],
                };




function download(content, fileName, contentType) {
    const a = document.createElement("a");
    const file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

function onDownload() {
    download(JSON.stringify(jsonData), "data.json", "text/plain");
}



// Canvas properties

let canvas = document.getElementById('thecanvas');
let context = canvas.getContext('2d');
let width = window.innerWidth;
let height = window.innerHeight;
context.canvas.width = width;
context.canvas.height = height;

// Variables 
// Global simulation config
let dt = 0.001; 
let simulationTime = 0;
let number_of_particles = 10;

// Start the simulation
let running = true;

// Simulation Functionality

// Checks if data is being recorded
let isRecording = false;

// Simulation Frame Speed
const fps = 120;
let stepsPerFrame = 25;



// LJ variables

let radius = 60; 

let epsilon =  100; 
let sigma = radius;


// Set the origin of the system
let origin_x = width/2 - 0.2*width;
let origin_y = height/2;


// Linear elastic potential

let k = document.getElementById("kSlider").value;
let amplitude = 0;


// Elastic potential source position

let x_0 = origin_x + (number_of_particles-1)*radius + 5 ;
let y_0 = origin_y;


// Set the slider to match the position of the elastic source
let springSlider = document.getElementById("springSlider");
springSlider.min = x_0;
springSlider.max = width;
springSlider.value = x_0;

// Particles  ----------------------------------------------------



// Properties
let molecularMass = 1; 
let mass = [];
let kneticEnergy = 0;

// Tracks the current position of the particles
let x = [];
let y = [];


let Acc_x = [];
let Acc_y = [];

//Make particles static
let isStatic = [];

// Generate initial simulation state
for (let i =0 ; i< number_of_particles ; i++){
    if (i < number_of_particles - 1){
        x.push(origin_x + i*radius + 5);
        
        
    }else{
        x.push(x_0 - amplitude);
    }
    mass.push(molecularMass)
    Acc_x.push(0)
    isStatic.push(false)

}

Acc_y = [...Acc_x];

for (let i =0 ; i<number_of_particles ; i++){
    y.push(origin_y )
}

// Tracks the previus position of the particles
let x_Prev = [...x];
let y_Prev = [...y];









// Functions -------------------------------------------------------------

function draw() {

    context.fillStyle = '#30638E ';
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);

    
    for (let i = 0; i < number_of_particles; i++) {

        drawCircles(i)
        //drawId(i)
    }
    drawElasticAtractor()
}

function drawId(i) {


    context.beginPath();
    context.font = "30px Comic Sans MS";
    context.fillStyle = 'hsl(0,100%,' + 90 + '%)';
    context.textAlign = "center";
    context.fillText(i + 1, x[i], y[i] + 10);
    context.closePath();
}

function drawCircles(i) {


    context.beginPath();
    context.arc(x[i], y[i], radius / 2, 0, 2 * Math.PI);
    //Make static particles black
    if(isStatic[i] == true){
        context.fillStyle = 'hsl(0,100%,' + 0 + '%)';
    }else{
        context.fillStyle = 'hsl(0,100%,' + 65 + '%)';
    }
    context.fill();
    context.closePath();
}

function drawElasticAtractor() {



    context.beginPath();
    context.arc(x_0, y_0, radius/8 , 0, 2 * Math.PI);
    context.fillStyle = 'hsl(0,100%,' + 0 + '%)';;
    context.fill();
    context.closePath();
}

function verlet() {
    var x_Next = 0;
    var y_Next = 0;

    // Make targeted particles static
    isStatic.forEach((state, i) => {
        if (state == true) {
            Acc_x[i] = 0;
            Acc_y[i] = 0;
            x_Prev[i] = x[i];
            y_Prev[i] = y[i]
        }
    })

    
    

    for (var i = 0; i < number_of_particles; i++) {
		
		//Verlet algorithm
        x_Next = 2 * x[i] - x_Prev[i] + Acc_x[i] * dt * dt;
        x_Prev[i] = x[i];
        x[i] = x_Next;

        
        //Periodic Boundary Conditions
        let aux = 3 / 4
        if (x[i] < -radius) {
            //  Lado esquerdo   
            x[i] = x[i] + width + radius * aux;
            x_Prev[i] = x_Prev[i] + width + radius * aux;

        } else if (x[i] > width + radius) {
            //  Lado direito
            x[i] = x[i] - width - radius * aux;
            x_Prev[i] = x_Prev[i] - width - radius * aux;
        }

        if (y[i] < -radius) {
            //  Cima
            y[i] = y[i] + height + radius * aux;
            y_Prev[i][i] = y_Prev[i][i] + height + radius * aux;
        } else if (y[i] > height) {
            //  Baixo
            y[i] = y[i] - height - radius * aux;
            y_Prev[i] = y_Prev[i] - height - radius * aux;
        }
    }

    computeAceleration()
}




function computeAceleration() {
	for( let i = 0 ; i < number_of_particles ; i++ ){
        Acc_x[i] = 0;
        Acc_y[i] = 0;
    }

    let dy, dx, r, v_1, Fij_x;
    for( let i = 0 ; i < number_of_particles; i++ ){
        for( let j = 0 ; j < number_of_particles; j++ ){
            if ((j!=i) && (j>i)){
                // Distance vector
    
                dy = y[i] - y[j];
                dx = x[i] - x[j];
                r = Math.sqrt(dx * dx + dy * dy);
                // Unit vector on x-axis
                v_1 = dx / r; 


                // Forces
                let attractiveForce = 2*((sigma)**12)*(1/(r**13));
                let repulsiveForce = ((sigma)**6)*(1 /( r** 7));

                Fij_x = -24 * epsilon * (attractiveForce - repulsiveForce);

                
                // Acelerations
                // x-axis
                
                Acc_x[i] += -(Fij_x / mass[i]) * v_1;
                Acc_x[j] += (Fij_x / mass[j]) * v_1;
            }
        }
    }

    // Calculate the distance from the elastic potential source to some particle
    let measuredParticleIndex = number_of_particles-1;
    let delta_x_spring = x_0 - x[measuredParticleIndex];

    // Add the elastic force to some specified particle
    Acc_x[measuredParticleIndex] += - (k * delta_x_spring / mass[measuredParticleIndex]) * v_1;


    simulationTime += dt;

	

    
    
    // Exporting data to JSON file

    if (isRecording) {
        jsonData['simulationTime'].push(simulationTime);
        jsonData['x'].push(x);
        jsonData['y'].push(y);
        jsonData['Acc_x'].push(Acc_x);
        jsonData['k'].push(k);
        jsonData['molecularMass'].push(molecularMass);
        jsonData['sigma'].push(sigma);
        jsonData['epsilon'].push(epsilon);
    }
}





// Functionality

function mouseHandler(){
    let mouseX = event.clientX;
    let mouseY = event.clientY;

    // Make particle static on click
    for (let i=0 ; i<number_of_particles; i++){
        if (Math.abs(mouseX - x[i]) < radius/2){
            isStatic[i] = !isStatic[i];
        }
    }
    
}

function recordData(){
    let recodDataButton = document.getElementById('recordDataButton');
    isRecording = !isRecording;
    if (isRecording){
        // Create a button to download the data dynamically
        if (!document.getElementById('downloadDataButton')){
            let recordOptionsContainer = document.getElementById('recordOptions')
            let button = document.createElement('button');
            button.type = 'button';
            button.innerHTML = 'Dowload Data';
            button.id = 'downloadDataButton'
            button.onclick = function (){
                onDownload();
            }
            recordOptionsContainer.appendChild(button)
        }
        
        recodDataButton.innerHTML = 'Stop Recording'
        console.log("Recording")
    }else{
        recodDataButton.innerHTML = 'Record Data'
        console.log("Stop Recording")
    }

}


function resize(){
    // Makes the canvas responsive
    context.canvas.width = window.innerWidth;
    context.canvas.height = window.innerHeight;
}

function updateK(){
    // Updates the spring constant
    k = document.getElementById("kSlider").value;
}

function moveSpring(){
    // Bind the slider with the position of the equilibrium point of the spring
    // allowing to move the coupled particle that has a black dot

    let springPos =  document.getElementById("springSlider").value;

    x_0 = springPos;    
    xOld_0 = springPos;

    x[number_of_particles-1] = springPos;
    x_Prev[number_of_particles-1] = springPos;
}


function simulate() {
    for (var step = 0; step < stepsPerFrame; step++) {
        // Move objects with verlet algorithm
        verlet()   
    }
    // Draws every object on the scene
    draw()
    if (running) {
        setTimeout(() => {
            requestAnimationFrame(simulate);
        }, 1000 / fps);
    }
}



